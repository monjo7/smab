# Setmana de música Antiga i Barroca
Pagina web multillenguatge
amb PHP, Css ,javaScript i tecnologies recients com React.

S'utilitzarà una Plantilla HTML5 amb disseny adaptatiu(responsive design)

## Client
Correu : **fran_rock_77@msn.com**

Nom del Projecte: "**Setmana de música Antiga i Barroca**"


El client te coneixements amb dominis y wordPressDisposa de targeta per pagar els els servicis necesaris per allotjar la web.

El client disposa d'una targeta per fer els pagaments del allotjament (servidor) i domini. Esta targeta se li cedirà al desarrollador per contractar els servicis.

Es un projecte a llarg plaç que interesa matenir actualizat i en constant millora.


## Breu descripció del projecte:
El projecte està dirigit a tots els públics, encara que al tractar-se d'una setmana de música antiga i barroca hem de tenir en compte que no a tot el món els agrada o consumixen eixe tipus de música i/o espectàcles.  Per tant, ens estarem centrant gent major d'edad, generalment.
El nom és ben senzill i obvi. "Setmana de música Antiga i Barroca" T'adjunte també un dels cartells de la primera edició.
La web no serà més que informativa (al menys de moment). Vull dir, s'anirien posant actualitzacions sobre grups confirmats, músics, etc. Seria, com he dit, més a mode d'informació. Estaria bé alguna part d'imatges o de la breu història de la setmana, edicions...

## La web contindrá les següents seccions:
* Blog (pagina principal)
* Quí som?
* Calendari de actuacións
* Imatges
* Breu històia
* Edicions pasades

*Es poden contemplar més opcions.*

## Contigut de la web:
Imatges, fotografies de qualitat, textos base i textos traduïts deuran ser realitzats per el client només començar el projecte!
La data máxima per a entregarlo serà a la meitat del projecte.


## Objectius de la primera entrega
El blog mostrarà les noticies que es publiquen en el facebook del projecte.
Mostrarà les últimes 10
Esta forma de fer-ho unifica les publicacions y facilita el proces de publicació del nou contingut.

Altra opció es fer un xicotet formulari en la web per publicar de forma manual les noves publicacions.
A este formulari s'entraria amb usuari i contrasenya.

Totes les seccions seràn multillenguatge (Valencià, Castellà i Anglés)

*Es poden contemplar més opcions*


## Pro i contres de no utilitzar wordPress
* pro
	- La seguretat es molt alta (No et poden piratejar)
	- Mantenirla es més facil
	- Es té el control total del desarrollament en tot moment.
	- La velocitat de la web és la més óptima (no hi ha que carregar coses que no s'utilitzen);
	- Es molt simple i fàcil d'utilitzar  
* Contra
	- Està molt estès





## Cronograma
Aproximació de com s'organitzarà el projecte:

![image](./Cronograma.png)



## Pagament
El projecte està pressupostat en 80h. L'hora es a 8€ sent el preu 640€

Es pagarà en 2 fraccions.
El 50% al començament del projecte i el restant al finalitzar.

Una volta em pasat les 2 setmanes de plantejament del projecte, els nous canvis es pressupostarán a banda,


## Coses a tindre en conter:


Si el desarrollador no acaba el projecte tornarà tots el diners al client.

Si el client no colabora amb les demandes del projecte dins del temps del cronograma, el desarrollador entregarà el códi sense que el projecte estiga finalitzat (sense tornar els diners).
